import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../entity/user';
import { UserService } from '../services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidator } from '../validator/password.validator';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  form: FormGroup;
  isLogged:Observable<boolean>;
  user:User = {username: null, email: null, password: null};
  feedback = '';

  constructor(private authServ:AuthenticationService, private userServ:UserService, private fb:FormBuilder) { }

  ngOnInit() {
    this.isLogged = this.authServ.user;
    this.form = this.fb.group({
      email: [
        '',
        [
          Validators.email,
          Validators.required
        ]
      ],
      password: ['', [Validators.minLength(4), Validators.required]],
      passwordRepeat: ['', Validators.required]
    },
    {
      //On applique un validateur sur tout le form group pour vérifier
      //si les deux champs password correspondent
      validator: PasswordValidator.passwordMatch('password', 'passwordRepeat')
    });


  }

  logout() {
    this.authServ.logout();
  }
  registered() {
    this.userServ.add(this.user).subscribe(
      () => this.feedback = 'Successfuly registered',
    () => 'Registration Error');

  }
}
