import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActionsService } from '../services/actions.service';
import { Actions } from '../entity/actions';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {
  
  operations:Actions = {name: null, comment: null, value: null, date: null};
  checked = false;

  constructor(private service:ActionsService) { }

  ngOnInit() {
  }

  onSubmit() {
    if(this.checked){
      this.operations.value = -this.operations.value;
    }
    this.service.addaction(this.operations).subscribe();


  }
}
