import { Component, OnInit } from '@angular/core';
import { Memo } from '../entity/memo';
import { MemoService } from '../services/memo.service';

@Component({
  selector: 'app-memo',
  templateUrl: './memo.component.html',
  styleUrls: ['./memo.component.css']
})
export class MemoComponent implements OnInit {

  memo: Memo = { name: null, comment: null, value: null, date: null };
  all: Memo[] = []
  edit = false;


  constructor(private service: MemoService) { }

  ngOnInit() {
    this.service.findAll().subscribe(data => this.all = data);
  }

  onSubmit() {
    if (this.edit) {
      this.service.update(this.memo).subscribe(data => {
        this.edit = false;
        this.memo = { name: null, comment: null, value: null, date: null };
      })
    }
    else {
      this.service.addmemo(this.memo).subscribe(
        () => { this.all.push(this.memo) }
      );
    }
  }

  clickDelete(obj) {

    this.service.delete(obj).subscribe(() => this.ngOnInit());
  }

  clickUpd(obj) {
    this.memo = obj;
    this.edit = true;
  }
}
