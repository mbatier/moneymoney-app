import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Memo } from '../entity/memo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MemoService {

  private url = 'http://localhost:8080/memo/'

  constructor(private http:HttpClient) { }

  findAll(): Observable<Memo[]> {
    return this.http.get<Memo[]>(this.url)
  
  }

  addmemo(newmemo): Observable<Memo[]> {
    return this.http.post<Memo[]>(this.url,newmemo)
  
  }

  delete(memo: Memo){
    return this.http.delete(`${this.url}${memo.id}`);
  }

  update(memo:Memo): Observable<Memo> {
    return this.http.put<Memo>(this.url + memo.id, memo);
  }

}