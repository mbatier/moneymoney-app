import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private url = 'http://localhost:8080/api/login_check';

  user = new BehaviorSubject<boolean>(false);

  constructor(private http:HttpClient) {
    
    if(localStorage.getItem('token')) {
      this.user.next(true);
    }
  }

  login(name:string, password:string): Observable<any> {
    return this.http.post<any>(this.url, {
      username:name, 
      password:password
    }).pipe(
     
      tap(response => {
        
        if(response.token) {
          localStorage.setItem('token', response.token);
          this.user.next(true);
        }
      })
    );
  }
  
  logout() {
    localStorage.removeItem('token');
    this.user.next(false);
  }
 
  isLoggedIn():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
}
