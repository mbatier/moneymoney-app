import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Actions } from '../entity/actions';


@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  private url = 'http://localhost:8080/actions/'

  constructor(private http:HttpClient) { }

  findAll(): Observable<Actions[]> {
    return this.http.get<Actions[]>(this.url)
  
  }

  addaction(newaction): Observable<Actions[]> {
    return this.http.post<Actions[]>(this.url,newaction)
  
  }

  delete(action: Actions){
    return this.http.delete(`${this.url}${action.id}`);
  }

}
