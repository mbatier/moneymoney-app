import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'http://localhost:8080/api/user/';

  constructor(private http:HttpClient) { }

  add(user:User):Observable<User> {
    return this.http.post<User>(this.url, user);
  }
}
