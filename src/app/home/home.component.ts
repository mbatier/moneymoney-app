import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  name = '';
  password = '';
  feedback = '';
  isLogged:Observable<boolean>;

  constructor(private authServ:AuthenticationService, private modalService:NgbModal) { }

  ngOnInit() {
    this.isLogged = this.authServ.user;
  }

  login() {
    this.authServ.login(this.name, this.password)
    .subscribe(() => { this.feedback = '';
    this.modalService.dismissAll();},
    (error) => {
      if(error.status === 401) {
        this.feedback = "Credentials error";
      } else {
        this.feedback = "Server error";
      };
      
    });
  }

  

  logged() {
    return this.authServ.isLoggedIn();
  }
}
