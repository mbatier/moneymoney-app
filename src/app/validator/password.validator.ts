import { ValidatorFn, AbstractControl, ValidationErrors } from "@angular/forms";


export class PasswordValidator {
    /**
     * Fonction de validation qui vérifie si 2 champs d'un formulaire
     * ont une valeur identique.
     * @param field1 le nom du premier champ
     * @param field2 le nom du second champ
     */
    static passwordMatch(field1:string, field2:string): ValidatorFn {
        //On fait renvoyer une fonction de validation à notre méthode
        //(on fait ça pour pouvoir assigner des paramètres à notre validateur)
        return (formGroup:AbstractControl): ValidationErrors | null => {
            //On récupère les deux champs du formulaire par leur nom
            const input1 = formGroup.get(field1);
            const input2 = formGroup.get(field2);
            //Si les deux champs ne correspondent pas
            if(input1.value !== input2.value) {
                //On met une erreur sur le second champ
                input2.setErrors({'passwordMatch':true})
                //Et on renvoie l'erreur déclenchée par ce validateur
                return {'passwordMatch': true};
            }
            //Sinon on renvoie null, ça veut dire qu'il y a pas d'erreur
            return null
        }
    }


}
