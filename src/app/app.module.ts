import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { ActionsComponent } from './actions/actions.component';
import { MemoComponent } from './memo/memo.component';
import { OperationsComponent } from './operations/operations.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AuthorizationInterceptor } from './services/interceptor/authorization.service';




const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'memo', component: MemoComponent },
  { path: 'actions', component: ActionsComponent },
  { path: 'operations', component: OperationsComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ActionsComponent,
    MemoComponent,
    OperationsComponent

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes
      // { enableTracing: true } // <-- debugging purposes only

    ),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
