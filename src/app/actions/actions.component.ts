import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActionsService } from '../services/actions.service';
import { Actions } from '../entity/actions';
import { Observable,from } from 'rxjs';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent implements OnInit {

  actions:Actions[] = [];

  constructor(private service:ActionsService) { }

  ngOnInit() {
    this.service.findAll().subscribe(data => this.actions = data);
  }

clickDelete(item) {
  
  this.service.delete(item).subscribe(() => this.ngOnInit());
}

}
