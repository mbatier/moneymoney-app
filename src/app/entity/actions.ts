export interface Actions {
    id?:number;
    name:string;
    value:number;
    comment:string;
    date:any;
}
