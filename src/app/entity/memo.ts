export interface Memo {
    id?:number;
    name:string;
    comment:string;
    value:number;
    date:any;
}
